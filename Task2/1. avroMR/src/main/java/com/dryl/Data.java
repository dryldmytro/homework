package com.dryl;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;
import org.apache.avro.Schema;
import org.apache.hadoop.io.WritableComparable;
//out class Data for parse Avro file with Schema and other lines
public class Data implements WritableComparable<Data> {
   Long id;
   String date_time;
   Integer site_name;
   Integer posa_continent;
   Integer user_location_country;
   Integer user_location_region;
   Integer user_location_city;
   Double orig_destination_distance;
   Integer user_id;
   Integer is_mobile;
   Integer is_package;
   Integer channel;
   String srch_ci;
   String srch_co;
   Integer srch_adults_cnt;
   Integer srch_children_cnt;
   Integer srch_rm_cnt;
   Integer srch_destination_id;
   Integer srch_destination_type_id;
   Long hotel_id;
  public static final Schema SCHEMA$ = new Schema.Parser().parse("{\n"
      + "  \"type\" : \"record\",\n"
      + "  \"name\" : \"topLevelRecord\",\n"
      + "  \"fields\" : [ {\n"
      + "    \"name\" : \"id\",\n"
      + "    \"type\" : [ \"long\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"date_time\",\n"
      + "    \"type\" : [ \"string\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"site_name\",\n"
      + "    \"type\" : [ \"int\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"posa_continent\",\n"
      + "    \"type\" : [ \"int\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"user_location_country\",\n"
      + "    \"type\" : [ \"int\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"user_location_region\",\n"
      + "    \"type\" : [ \"int\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"user_location_city\",\n"
      + "    \"type\" : [ \"int\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"orig_destination_distance\",\n"
      + "    \"type\" : [ \"double\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"user_id\",\n"
      + "    \"type\" : [ \"int\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"is_mobile\",\n"
      + "    \"type\" : [ \"int\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"is_package\",\n"
      + "    \"type\" : [ \"int\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"channel\",\n"
      + "    \"type\" : [ \"int\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"srch_ci\",\n"
      + "    \"type\" : [ \"string\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"srch_co\",\n"
      + "    \"type\" : [ \"string\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"srch_adults_cnt\",\n"
      + "    \"type\" : [ \"int\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"srch_children_cnt\",\n"
      + "    \"type\" : [ \"int\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"srch_rm_cnt\",\n"
      + "    \"type\" : [ \"int\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"srch_destination_id\",\n"
      + "    \"type\" : [ \"int\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"srch_destination_type_id\",\n"
      + "    \"type\" : [ \"int\", \"null\" ]\n"
      + "  }, {\n"
      + "    \"name\" : \"hotel_id\",\n"
      + "    \"type\" : [ \"long\", \"null\" ]\n"
      + "  } ]\n"
      + "}");

  public Data() {
  }

  public Data(Long id, String date_time, Integer site_name, Integer posa_continent,
      Integer user_location_country, Integer user_location_region,
      Integer user_location_city, double orig_destination_distance, Integer user_id,
      Integer is_mobile, Integer is_package, Integer channel, String srch_ci,
      String srch_co, Integer srch_adults_cnt, Integer srch_children_cnt,
      Integer srch_rm_cnt, Integer srch_destination_id, Integer srch_destination_type_id,
      Long hotel_id) {
    this.id = id;
    this.date_time = date_time;
    this.site_name = site_name;
    this.posa_continent = posa_continent;
    this.user_location_country = user_location_country;
    this.user_location_region = user_location_region;
    this.user_location_city = user_location_city;
    this.orig_destination_distance = orig_destination_distance;
    this.user_id = user_id;
    this.is_mobile = is_mobile;
    this.is_package = is_package;
    this.channel = channel;
    this.srch_ci = srch_ci;
    this.srch_co = srch_co;
    this.srch_adults_cnt = srch_adults_cnt;
    this.srch_children_cnt = srch_children_cnt;
    this.srch_rm_cnt = srch_rm_cnt;
    this.srch_destination_id = srch_destination_id;
    this.srch_destination_type_id = srch_destination_type_id;
    this.hotel_id = hotel_id;
  }

  public static Schema getClassSchema() {
    return SCHEMA$;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getDate_time() {
    return date_time;
  }

  public void setDate_time(String date_time) {
    this.date_time = date_time;
  }

  public Integer getSite_name() {
    return site_name;
  }

  public void setSite_name(Integer site_name) {
    this.site_name = site_name;
  }

  public Integer getPosa_continent() {
    return posa_continent;
  }

  public void setPosa_continent(Integer posa_continent) {
    this.posa_continent = posa_continent;
  }

  public Integer getUser_location_country() {
    return user_location_country;
  }

  public void setUser_location_country(Integer user_location_country) {
    this.user_location_country = user_location_country;
  }

  public Integer getUser_location_region() {
    return user_location_region;
  }

  public void setUser_location_region(Integer user_location_region) {
    this.user_location_region = user_location_region;
  }

  public Integer getUser_location_city() {
    return user_location_city;
  }

  public void setUser_location_city(Integer user_location_city) {
    this.user_location_city = user_location_city;
  }

  public Double getOrig_destination_distance() {
    return orig_destination_distance;
  }

  public void setOrig_destination_distance(Double orig_destination_distance) {
    this.orig_destination_distance = orig_destination_distance;
  }

  public Integer getUser_id() {
    return user_id;
  }

  public void setUser_id(Integer user_id) {
    this.user_id = user_id;
  }

  public Integer getIs_mobile() {
    return is_mobile;
  }

  public void setIs_mobile(Integer is_mobile) {
    this.is_mobile = is_mobile;
  }

  public Integer getIs_package() {
    return is_package;
  }

  public void setIs_package(Integer is_package) {
    this.is_package = is_package;
  }

  public Integer getChannel() {
    return channel;
  }

  public void setChannel(Integer channel) {
    this.channel = channel;
  }

  public String getSrch_ci() {
    return srch_ci;
  }

  public void setSrch_ci(String srch_ci) {
    this.srch_ci = srch_ci;
  }

  public String getSrch_co() {
    return srch_co;
  }

  public void setSrch_co(String srch_co) {
    this.srch_co = srch_co;
  }

  public Integer getSrch_adults_cnt() {
    return srch_adults_cnt;
  }

  public void setSrch_adults_cnt(Integer srch_adults_cnt) {
    this.srch_adults_cnt = srch_adults_cnt;
  }

  public Integer getSrch_children_cnt() {
    return srch_children_cnt;
  }

  public void setSrch_children_cnt(Integer srch_children_cnt) {
    this.srch_children_cnt = srch_children_cnt;
  }

  public Integer getSrch_rm_cnt() {
    return srch_rm_cnt;
  }

  public void setSrch_rm_cnt(Integer srch_rm_cnt) {
    this.srch_rm_cnt = srch_rm_cnt;
  }

  public Integer getSrch_destination_id() {
    return srch_destination_id;
  }

  public void setSrch_destination_id(Integer srch_destination_id) {
    this.srch_destination_id = srch_destination_id;
  }

  public Integer getSrch_destination_type_id() {
    return srch_destination_type_id;
  }

  public void setSrch_destination_type_id(Integer srch_destination_type_id) {
    this.srch_destination_type_id = srch_destination_type_id;
  }

  public Long getHotel_id() {
    return hotel_id;
  }

  public void setHotel_id(Long hotel_id) {
    this.hotel_id = hotel_id;
  }

  public static Schema getSCHEMA$() {
    return SCHEMA$;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Data)) {
      return false;
    }
    Data data = (Data) o;
    return Objects.equals(getId(), data.getId()) &&
        Objects.equals(getDate_time(), data.getDate_time()) &&
        Objects.equals(getSite_name(), data.getSite_name()) &&
        Objects.equals(getPosa_continent(), data.getPosa_continent()) &&
        Objects.equals(getUser_location_country(), data.getUser_location_country()) &&
        Objects.equals(getUser_location_region(), data.getUser_location_region()) &&
        Objects.equals(getUser_location_city(), data.getUser_location_city()) &&
        Objects.equals(getOrig_destination_distance(), data.getOrig_destination_distance())
        &&
        Objects.equals(getUser_id(), data.getUser_id()) &&
        Objects.equals(getIs_mobile(), data.getIs_mobile()) &&
        Objects.equals(getIs_package(), data.getIs_package()) &&
        Objects.equals(getChannel(), data.getChannel()) &&
        Objects.equals(getSrch_ci(), data.getSrch_ci()) &&
        Objects.equals(getSrch_co(), data.getSrch_co()) &&
        Objects.equals(getSrch_adults_cnt(), data.getSrch_adults_cnt()) &&
        Objects.equals(getSrch_children_cnt(), data.getSrch_children_cnt()) &&
        Objects.equals(getSrch_rm_cnt(), data.getSrch_rm_cnt()) &&
        Objects.equals(getSrch_destination_id(), data.getSrch_destination_id()) &&
        Objects.equals(getSrch_destination_type_id(), data.getSrch_destination_type_id()) &&
        Objects.equals(getHotel_id(), data.getHotel_id());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId(), getDate_time(), getSite_name(), getPosa_continent(),
        getUser_location_country(), getUser_location_region(), getUser_location_city(),
        getOrig_destination_distance(), getUser_id(), getIs_mobile(), getIs_package(), getChannel(),
        getSrch_ci(), getSrch_co(), getSrch_adults_cnt(), getSrch_children_cnt(), getSrch_rm_cnt(),
        getSrch_destination_id(), getSrch_destination_type_id(), getHotel_id());
  }

  @Override
  public String toString() {
    return "Data{" +
        "id=" + id +
        ", date_time=" + date_time +
        ", site_name=" + site_name +
        ", posa_continent=" + posa_continent +
        ", user_location_country=" + user_location_country +
        ", user_location_region=" + user_location_region +
        ", user_location_city=" + user_location_city +
        ", orig_destination_distance=" + orig_destination_distance +
        ", user_id=" + user_id +
        ", is_mobile=" + is_mobile +
        ", is_package=" + is_package +
        ", channel=" + channel +
        ", srch_ci=" + srch_ci +
        ", srch_co=" + srch_co +
        ", srch_adults_cnt=" + srch_adults_cnt +
        ", srch_children_cnt=" + srch_children_cnt +
        ", srch_rm_cnt=" + srch_rm_cnt +
        ", srch_destination_id=" + srch_destination_id +
        ", srch_destination_type_id=" + srch_destination_type_id +
        ", hotel_id=" + hotel_id +
        '}';
  }

    @Override
  public int compareTo(Data o) {
      int cmt = id.compareTo(o.id);
      if (cmt!=0){
        return cmt;
      }
      cmt = date_time.compareTo(o.date_time);
      if (cmt!=0){
        return cmt;
      }
      cmt = site_name.compareTo(o.site_name);
      if (cmt!=0){
        return cmt;
      }
      cmt = posa_continent.compareTo(o.posa_continent);
      if (cmt!=0){
        return cmt;
      }
      cmt = user_location_country.compareTo(o.user_location_country);
      if (cmt!=0){
        return cmt;
      }
      cmt = user_location_region.compareTo(o.user_location_region);
      if (cmt!=0){
        return cmt;
      }
      cmt = user_location_city.compareTo(o.user_location_city);
      if (cmt!=0){
        return cmt;
      }
      cmt = user_id.compareTo(o.user_id);
      if (cmt!=0){
        return cmt;
      }
      cmt = is_mobile.compareTo(o.is_mobile);
      if (cmt!=0){
        return cmt;
      }
      cmt = is_package.compareTo(o.is_package);
      if (cmt!=0){
        return cmt;
      }
      cmt = channel.compareTo(o.channel);
      if (cmt!=0){
        return cmt;
      }
      cmt = srch_ci.compareTo(o.srch_ci);
      if (cmt!=0){
        return cmt;
      }
      cmt = srch_co.compareTo(srch_co);
      if (cmt!=0){
        return cmt;
      }
      cmt = srch_adults_cnt.compareTo(o.srch_adults_cnt);
      if (cmt!=0){
        return cmt;
      }
      cmt = srch_children_cnt.compareTo(o.srch_children_cnt);
      if (cmt!=0){
        return cmt;
      }
      cmt = srch_rm_cnt.compareTo(o.srch_rm_cnt);
      if (cmt!=0){
        return cmt;
      }
      cmt = srch_destination_id.compareTo(o.srch_destination_id);
      if (cmt!=0){
        return cmt;
      }
      cmt = srch_destination_type_id.compareTo(o.srch_destination_type_id);
      if (cmt!=0){
        return cmt;
      }
      return hotel_id.compareTo(o.hotel_id);
  }

  @Override
  public void write(DataOutput dataOutput) throws IOException {
    dataOutput.writeLong(id);
    dataOutput.writeUTF(date_time);
    dataOutput.writeInt(site_name);
    dataOutput.writeInt(posa_continent);
    dataOutput.writeInt(user_location_country);
    dataOutput.writeInt(user_location_region);
    dataOutput.writeInt(user_location_city);
    dataOutput.writeDouble(orig_destination_distance);
    dataOutput.writeInt(user_id);
    dataOutput.writeInt(is_mobile);
    dataOutput.writeInt(is_package);
    dataOutput.writeInt(channel);
    dataOutput.writeUTF(srch_ci);
    dataOutput.writeUTF(srch_co);
    dataOutput.writeInt(srch_adults_cnt);
    dataOutput.writeInt(srch_children_cnt);
    dataOutput.writeInt(srch_rm_cnt);
    dataOutput.writeInt(srch_destination_id);
    dataOutput.writeInt(srch_destination_type_id);
    dataOutput.writeLong(hotel_id);
  }

  @Override
  public void readFields(DataInput dataInput) throws IOException {
    id = dataInput.readLong();
    date_time = dataInput.readUTF();
    site_name = dataInput.readInt();
    posa_continent = dataInput.readInt();
    user_location_country = dataInput.readInt();
    user_location_region = dataInput.readInt();
    user_location_city = dataInput.readInt();
    orig_destination_distance = dataInput.readDouble();
    user_id = dataInput.readInt();
    is_mobile = dataInput.readInt();
    is_package = dataInput.readInt();
    channel = dataInput.readInt();
    srch_ci = dataInput.readUTF();
    srch_co = dataInput.readUTF();
    srch_adults_cnt= dataInput.readInt();
    srch_children_cnt = dataInput.readInt();
    srch_rm_cnt = dataInput.readInt();
    srch_destination_id = dataInput.readInt();
    srch_destination_type_id = dataInput.readInt();
    hotel_id = dataInput.readLong();
  }
}
