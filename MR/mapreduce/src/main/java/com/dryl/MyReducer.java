package com.dryl;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class MyReducer extends Reducer<Text, IntWritable,Text,IntWritable> {
  public void reduce(Text text,Iterable<IntWritable>iterable,Context context)
      throws IOException, InterruptedException {
    int sum = 0;
    for (IntWritable value:iterable) {
      sum+=value.get();
    }
    context.write(text,new IntWritable(sum));
  }
}
