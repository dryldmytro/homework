package com.dryl;

import java.sql.Timestamp;

public class UserData {
  String user;
  String activity;
  Timestamp start;
  Timestamp end;

  public UserData() {
  }

  public UserData(String user, String activity, Timestamp start, Timestamp end) {
    this.user = user;
    this.activity = activity;
    this.start = start;
    this.end = end;
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public String getActivity() {
    return activity;
  }

  public void setActivity(String activity) {
    this.activity = activity;
  }

  public Timestamp getStart() {
    return start;
  }

  public void setStart(Timestamp start) {
    this.start = start;
  }

  public Timestamp getEnd() {
    return end;
  }

  public void setEnd(Timestamp end) {
    this.end = end;
  }

  @Override
  public String toString() {
    return "OneMore{" +
        "name='" + user + '\'' +
        ", activity='" + activity + '\'' +
        ", start=" + start +
        ", end=" + end +
        '}';
  }
}
