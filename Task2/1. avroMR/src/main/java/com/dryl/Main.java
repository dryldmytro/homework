package com.dryl;

import java.io.IOException;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapreduce.AvroJob;
import org.apache.avro.mapreduce.AvroKeyInputFormat;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class Main extends Configured implements Tool {

  static class MyMapper extends Mapper<AvroKey<GenericRecord>, NullWritable, Data, NullWritable> {

    //parse data in out Data class
    @Override
    protected void map(AvroKey<GenericRecord> key, NullWritable value, Context context)
        throws IOException, InterruptedException {
      Long id = (Long) key.datum().get(0);
      String date_time = key.datum().get(1).toString();
      Integer site_name = (Integer) key.datum().get(2);
      Integer posa_continent = (Integer) key.datum().get(3);
      Integer user_location_country = (Integer) key.datum().get(4);
      Integer user_location_region = (Integer) key.datum().get(5);
      Integer user_location_city = (Integer) key.datum().get(6);
      Object o = key.datum().get(7);
      if (null == o){
        o = "0.0";
      }
      Double orig_destination_distance = Double.parseDouble(o.toString());
      Integer user_id = (Integer) key.datum().get(8);
      Integer is_mobile = (Integer) key.datum().get(9);
      Integer is_package = (Integer) key.datum().get(10);
      Integer channel = (Integer) key.datum().get(11);
      String srch_ci = key.datum().get(12).toString();
      String srch_co = key.datum().get(13).toString();
      Integer srch_adults_cnt = (Integer) key.datum().get(14);
      Integer srch_children_cnt = (Integer) key.datum().get(15);
      Integer srch_rm_cnt = (Integer) key.datum().get(16);
      Integer srch_destination_id = (Integer) key.datum().get(17);
      Integer srch_destination_type_id = (Integer) key.datum().get(18);
      Long hotel_id = (Long) key.datum().get(19);
      Data data = new Data(id, date_time, site_name, posa_continent,
          user_location_country, user_location_region, user_location_city, orig_destination_distance,
          user_id, is_mobile, is_package, channel, srch_ci, srch_co, srch_adults_cnt,
          srch_children_cnt,
          srch_rm_cnt, srch_destination_id, srch_destination_type_id, hotel_id);
      //we need hotels just where adults_cnt>2
      if (data.getSrch_adults_cnt()>=2) {
        context.write(data, NullWritable.get());
//        context.write(new LongWritable(data.getHotel_id()), data);   - code for second task
      }
    }
  }

  static class MyReducer extends Reducer<Data, NullWritable, Data, NullWritable> {

    //get data from mapper and write in output context
    @Override
    protected void reduce(Data key, Iterable<NullWritable> values, Context context)
        throws IOException, InterruptedException {
      context.write(key, NullWritable.get());

      //code for second task (2.Find marketing channel for each hotel_id with the latest companies visit.)
//      Data data1 = new Data();
//      for (Data d:values) {
//        data = d;
//      }
//      context.write(data,new IntWritable(data.getChannel()));

    }
  }

  @Override
  public int run(String[] strings) throws Exception {
    //all main settings
    Job job = new Job(getConf());
    job.setJarByClass(Main.class);
    job.setJobName("Avro");

    FileInputFormat.setInputPaths(job, new Path(strings[0]));
    FileOutputFormat.setOutputPath(job, new Path(strings[1]));

    job.setInputFormatClass(AvroKeyInputFormat.class);
    job.setMapperClass(MyMapper.class);
    job.setPartitionerClass(CustomPartitioner.class);
    job.setSortComparatorClass(CustomSortComparator.class);
    job.setGroupingComparatorClass(GroupingComparator.class);

    AvroJob.setInputKeySchema(job, Data.getClassSchema());
    job.setMapOutputKeyClass(Data.class);
    job.setMapOutputValueClass(NullWritable.class);

    job.setReducerClass(MyReducer.class);

    return (job.waitForCompletion(true) ? 0 : 1);
  }

  public static void main(String[] args) throws Exception {
    //start app
    int res = ToolRunner.run(new Main(), args);
    System.exit(res);
  }
}
