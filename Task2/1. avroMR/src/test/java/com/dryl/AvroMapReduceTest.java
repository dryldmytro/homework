package com.dryl;

import com.dryl.Main.MyMapper;
import com.dryl.Main.MyReducer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.mapred.AvroKey;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

public class AvroMapReduceTest {
  MapDriver<AvroKey<GenericRecord>, NullWritable, Data, NullWritable> mapDriver;

  ReduceDriver<Data, NullWritable, Data, NullWritable> reduceDriver;
  MapReduceDriver<AvroKey<GenericRecord>, NullWritable, Data, NullWritable, Data, NullWritable> mapReduceDriver;

  @Before
  public void setup() throws IOException {
    Main.MyMapper myMapper = new MyMapper();
    mapDriver = new MapDriver<>();
    mapDriver.setMapper(myMapper);

    Main.MyReducer myReducer = new MyReducer();
    reduceDriver = new ReduceDriver<>();
    reduceDriver.setReducer(myReducer);

    mapReduceDriver = MapReduceDriver.newMapReduceDriver();
    mapReduceDriver.setMapper(myMapper);
    mapReduceDriver.setReducer(myReducer);
  }

  @Test
  public void mapTest() throws IOException {
    mapDriver.withInput(new AvroKey<>(),NullWritable.get());
    mapDriver.withOutput(new Data(1L,"",1,1,1,1,1,1.0,1,1,1,1,"","",1,1,1,1,1,1L),NullWritable.get());
    mapDriver.runTest();
  }

  @Test
  public void reducerTest() throws IOException {
    List<Data> list = new ArrayList<>();
    List<NullWritable> l = new ArrayList<>();
    l.add(NullWritable.get());
    l.add(NullWritable.get());
    list.add(new Data(1L,"",1,1,1,1,1,1.0,1,1,1,1,"","",1,1,1,1,1,1L));
    list.add(new Data(1L,"",1,1,1,1,1,1.0,1,1,1,1,"","",1,1,1,1,1,2L));
    reduceDriver.withInput(list.get(0), l);
    reduceDriver.withOutput(list.get(0),NullWritable.get());
    reduceDriver.runTest();

  }

  @Test
  public void allTest() throws IOException {
    mapReduceDriver.withInput(new AvroKey<>(),NullWritable.get())
        .withOutput(new Data(1L,"",1,1,1,1,1,1.0,1,1,1,1,"","",1,1,1,1,1,1L),NullWritable.get())
        .runTest();
  }
}
