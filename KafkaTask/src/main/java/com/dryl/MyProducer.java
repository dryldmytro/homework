package com.dryl;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.Scanner;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class MyProducer {
  private static String name = null;
  private static final Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) {
    KafkaProducer<String,String> kafkaProducer = getProducer();
    getName();
    while (true) {
      System.out.println("Enter your activity: ");
      String activity = scanner.nextLine();
      Timestamp time = new Timestamp(System.currentTimeMillis());
      ProducerRecord<String, String> record = new ProducerRecord<>
          ("Activity", name, time+activity);
      kafkaProducer.send(record);
    }
  }

  public static void getName(){
    while (name==null){
      System.out.println("Enter your name: ");
      String scn = scanner.nextLine();
      if (scn.length()>0){
        name=scn;
      }else {
        System.out.println("The name is too short!");
      }
    }
  }
  public static KafkaProducer<String, String> getProducer(){
    Properties kafkaProps = new Properties();
    kafkaProps.put("bootstrap.servers", "localhost:9092");
    kafkaProps.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
    kafkaProps.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
    KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(kafkaProps);
    return kafkaProducer;
  }
}
