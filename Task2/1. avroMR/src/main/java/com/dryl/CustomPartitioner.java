package com.dryl;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Partitioner;
//our custom partitioner
public class CustomPartitioner extends Partitioner<Data, NullWritable> {

  @Override
  public int getPartition(Data data, NullWritable nullWritable, int i) {
    return Math.abs(data.getId().hashCode())%i;
  }
}
