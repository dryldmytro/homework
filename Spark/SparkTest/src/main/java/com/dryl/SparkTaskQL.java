package com.dryl;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class SparkTaskQL {

  public static void main(String[] args) {
    //configure Session
    final SparkConf conf = new SparkConf().setAppName("spark-app").setMaster("local[*]");
    final SparkSession spark = SparkSession.builder().appName("SparkDS").config(conf).getOrCreate();
    //read files and parse them to SparkSQL
    spark.read().format("csv").option("header","true").load(args[0])
        .createOrReplaceTempView("master");
    spark.read().format("csv").option("header","true").load(args[1])
        .createOrReplaceTempView("goalies");
    spark.read().format("csv").option("header","true").load(args[2])
        .createOrReplaceTempView("awardsPlayer");
    spark.read().format("csv").option("header","true").load(args[3])
        .createOrReplaceTempView("teams");
    //make requests
    spark.sql("SELECT master.playerID, master.firstName,master.lastName,tt.sum FROM master "
        + "join (SELECT playerID,sum(PostGA) as sum FROM goalies group by playerID order by sum(PostGA) DESC limit 10) as tt"
        + " ON master.playerID=tt.playerID").createOrReplaceTempView("topTen");

    spark.sql("SELECT g.playerID,t.name FROM goalies as g "
        + "JOIN (SELECT tmID, name FROM teams) as t ON g.tmID=t.tmID "
        + "GROUP BY g.playerID,t.name")
        .createOrReplaceTempView("teamNames");

    spark.sql("SELECT tt.playerID, tt.firstName,tt.lastName,tt.sum,concat_ws(', ',collect_list(tn.name)) as teams FROM topTen "
        + "as tt JOIN teamNames as tn ON tt.playerID=tn.playerID"
        + " group by tt.playerID,tt.firstName,tt.lastName,tt.sum").createOrReplaceTempView("topWithTeams");

    spark.sql("SELECT playerID,count(award)as awards FROM awardsPlayer group by playerID")
    .createOrReplaceTempView("awards");

    Dataset<Row> finalResult=spark.sql("SELECT twt.firstName,twt.lastName,twt.sum as goals,twt.teams,awards.awards "
        + "FROM topWithTeams as twt join awards on twt.playerID=awards.playerID order by goals DESC");
    finalResult.coalesce(1).write().option("header","true").csv(args[4]);

    spark.close();
  }
}
