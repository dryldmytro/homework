package com.dryl;

import java.util.ArrayList;
import java.util.List;
import org.apache.spark.api.java.function.FlatMapGroupsWithStateFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.streaming.GroupStateTimeout;
import org.apache.spark.sql.streaming.OutputMode;
import org.apache.spark.sql.streaming.StreamingQueryException;

public class NewMain {

  public static void main(String[] args) throws StreamingQueryException, InterruptedException {
    SparkSession sparkSession = SparkSession.builder().appName("app").master("local[*]")
        .getOrCreate();

    sparkSession.sparkContext().setLogLevel("ERROR");

    Dataset<UserActivity> ds = sparkSession.readStream()
        .format("kafka").option("kafka.bootstrap.servers", "localhost:9092")
        .option("subscribe", "Activity")
        .load()
        .selectExpr("CAST(key AS STRING) as user", "CAST(substring(value,24) AS STRING) as activity",
            "cast(substring(value,0,23) as STRING) as timestamp")
        .selectExpr("user","activity","cast(timestamp as timestamp) as timestamp")
        .as(Encoders.bean(UserActivity.class))
        .withWatermark("timestamp", "1 minutes")
        .dropDuplicates();

    Dataset<UserData> ds1 = ds
        .groupByKey((MapFunction<UserActivity, String>) UserActivity::getUser, Encoders.STRING())
        .flatMapGroupsWithState(
            getFlatMap()
            , OutputMode.Append(), Encoders.bean(UserData.class), Encoders.bean(
                UserData.class),
            GroupStateTimeout.EventTimeTimeout());

    Dataset<Row> ds2 = ds.distinct().groupBy(functions.window(ds.col("timestamp"),
        "1 minutes","1 minutes"),
        ds.col("user")).count();

    ds1.writeStream().queryName("abc").outputMode("append").format("console")
        .option("truncate", false).start();
    ds2.writeStream().outputMode("complete").format("console").option("truncate", false).start()
        .awaitTermination();
  }

  public static FlatMapGroupsWithStateFunction getFlatMap() {
    FlatMapGroupsWithStateFunction<String, UserActivity, UserData, UserData> FM = (s, iterator, groupState) -> {
      List<UserData> list = new ArrayList<>();
      UserActivity userActivity = iterator.next();
      UserData userData = new UserData(userActivity.getUser(), userActivity.getActivity(),
          userActivity.getTimestamp(), null);
      if (groupState.exists()) {
        UserData userData1 = groupState.get();
        if (userData1.getActivity().equals(userData.getActivity())) {
          list.add(userData1);
          return list.iterator();
        }
        userData1.setEnd(userActivity.getTimestamp());
        groupState.update(userData);
        list.add(userData);
        list.add(userData1);
        return list.iterator();
      } else {
        groupState.update(userData);
        list.add(userData);
        return list.iterator();
      }
    };
    return FM;
  }


}
