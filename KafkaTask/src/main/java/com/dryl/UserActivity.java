package com.dryl;

import java.io.Serializable;
import java.sql.Timestamp;

public class UserActivity implements Serializable {
  private Timestamp timestamp;
  private String user;
  private String activity;


  public Timestamp getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Timestamp timestamp) {
    this.timestamp = timestamp;
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public String getActivity() {
    return activity;
  }

  public void setActivity(String activity) {
    this.activity = activity;
  }

  @Override
  public String toString() {
    return "ActivityTable{" +
        "start=" + timestamp +
        ", name='" + user + '\'' +
        ", activity='" + activity + '\'' +
        '}';
  }
}
