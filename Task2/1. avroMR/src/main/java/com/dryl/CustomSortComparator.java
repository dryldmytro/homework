package com.dryl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
//our comparator for secondary sort
public class CustomSortComparator extends WritableComparator {
  private CustomSortComparator(){
    super(Data.class,true);
  }

  @Override
  public int compare(WritableComparable a, WritableComparable b) {
    Data data1 = (Data) a;
    Data data2 = (Data) b;
    int compare = data1.getHotel_id().compareTo(data2.getHotel_id());
    if (compare == 0){
      SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");
      try {
        Date docDate1 = format.parse(data1.getSrch_ci());
        Date docDate2 = format.parse(data2.getSrch_ci());
        compare = docDate1.compareTo(docDate2);
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }
    if (compare==0){
      compare = data1.getId().compareTo(data2.getId());
    }
    return compare;
  }
}
