package com.dryl;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
//main comparator
public class GroupingComparator extends WritableComparator {
  protected GroupingComparator(){
    super(Data.class,true);
  }

  @Override
  public int compare(WritableComparable a, WritableComparable b) {
    Data data1 = (Data) a;
    Data data2 = (Data) b;
    return data1.compareTo(data2);
  }
}
