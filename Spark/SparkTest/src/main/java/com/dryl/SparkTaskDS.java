package com.dryl;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.*;

public class SparkTaskDS {

  public static void main(String[] args) {
    //configure Session
    final SparkConf conf = new SparkConf().setAppName("spark-app");
    final SparkSession spark = SparkSession.builder().appName("SparkDS").config(conf).getOrCreate();
    //read files and parse them to SparkSQL
    Dataset<Row> master = spark.read().format("csv").option("header","true").option("inferSchema","true")
        .load(args[0]);
    Dataset<Row> goalies = spark.read().format("csv").option("header","true").option("inferSchema","true")
        .load(args[1]);
    Dataset<Row> awards = spark.read().format("csv").option("header","true").option("inferSchema","true")
        .load(args[2]);
    Dataset<Row> teams = spark.read().format("csv").option("header","true").option("inferSchema","true")
        .load(args[3]);

    //operations
    Dataset<Row> topTen = goalies.groupBy("playerID").agg(sum("PostGA").as("PostGA"))
        .orderBy(desc("PostGA")).limit(10);
    Dataset<Row> topTenWithNames = master.select("playerID","firstName","lastName")
        .join(topTen,"playerID");
    Dataset<Row> playerTeams = goalies.join(teams, "tmID").select("playerID", "name")
        .distinct().groupBy("playerID").agg(collect_list("name").as("teams"));
    Dataset<Row> playerAward = awards.groupBy("playerID").agg(count("playerID").as("awards"));
    topTenWithNames.join(playerTeams,"playerID").join(playerAward,"playerID")
    .select("firstName","lastName","PostGA","teams","awards").orderBy(desc("PostGA"))
        .coalesce(1).write().json(args[4]);
    spark.close();
  }
}
