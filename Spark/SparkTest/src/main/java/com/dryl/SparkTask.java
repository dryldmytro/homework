package com.dryl;

import java.util.List;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

public class SparkTask {

  public static void main(String[] args) {
    final SparkConf conf = new SparkConf()
        .setAppName("spark-app")
        .setMaster("local[*]");

    final JavaSparkContext sparkContext = new JavaSparkContext(conf);

    final JavaRDD<String> awardsPlayer = sparkContext.textFile(args[0]);
    final JavaRDD<String> master = sparkContext.textFile(args[1]);
    final JavaRDD<String> goalies = sparkContext.textFile(args[2]);
    final JavaRDD<String> teams = sparkContext.textFile(args[3]);

    final JavaPairRDD<String,Long> onlyPlayerIdAndAward = awardsPlayer.mapToPair(
        line -> {   String[] values = line.split(",");
          return new Tuple2<>(values[0],1L);
        });

    final JavaPairRDD<String,String> onlyPlayerAndFullName = master.mapToPair(
        line -> {
          String[] values = line.split(",",-1);
          String playerid = "";
          for (String id : List.of(values[0])){
            if (!id.equals("")){
              playerid = id;
              break;
            }
          }
          if (playerid.equals("")){
            playerid = "unknown";
          }
          return new Tuple2<>(playerid,String.join(" ",values[3],values[4]));
        });

    final JavaPairRDD<String,Integer> idAndGoals = goalies.mapToPair(
        line ->{
          String[] values = line.split(",",-1);
          String s = "";
          for (String s1:List.of(values[21])){
            if (!s1.equals("")){
              s=s1;
              break;
            }
          }
          if (s.equals("") || s.equals("PostGA")){
            s="0";
          }
          Integer l = Integer.valueOf(s);
          return new Tuple2<>(values[0],l);
        }
    );
    final List<Tuple2<Integer, String>> idWithAllGoals = idAndGoals.reduceByKey((x,y)->x+y)
        .mapToPair(t -> new Tuple2<>(t._2, t._1))
        .sortByKey(false).take(10);
    JavaPairRDD<String,Integer> sortedGoals = sparkContext.parallelizePairs(idWithAllGoals)
        .mapToPair(t -> new Tuple2<>(t._2, t._1));

    final JavaPairRDD<String, Long> playerIdAndCountAwards = onlyPlayerIdAndAward.reduceByKey((x,y)->x+y);

    JavaPairRDD<String,Tuple2<String,Integer>> joinedPlayerAndGoals= onlyPlayerAndFullName.join(sortedGoals);

    final JavaPairRDD<String,String> playerAndTeamId = goalies.mapToPair(
        line ->{
          String[] values = line.split(",",-1);
          return new Tuple2<>(values[0],values[3]);
        }
    );

    final JavaPairRDD<String,String> teamIdAndPlayer= playerAndTeamId.mapToPair(t -> new Tuple2<>(t._2, t._1));

    final JavaPairRDD<String,String> teamsRdd = teams.mapToPair(
        line -> {
          String[] values = line.split(",",-1);
          return new Tuple2<>(values[2],values[18]);
        });

    final JavaPairRDD<String, Iterable<String>> abc= teamIdAndPlayer.join(teamsRdd)
        .mapToPair(t -> new Tuple2<>(t._2._1, t._2._2)).distinct().groupByKey();

    final JavaPairRDD<String, Tuple2<Tuple2<String, Integer>, Iterable<String>>> pairRddwithoutAwards = joinedPlayerAndGoals.join(abc);

    final JavaPairRDD<String, Tuple2<Tuple2<Tuple2<String, Integer>, Iterable<String>>, Long>> finalPairRdd = pairRddwithoutAwards.join(playerIdAndCountAwards);
    final JavaRDD<String> javaRDD = finalPairRdd.map(x->x._2.toString());
    javaRDD.coalesce(1,true).saveAsTextFile(args[4]);
    sparkContext.close();

  }
}
